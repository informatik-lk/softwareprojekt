public class Main {
    public static void main(String[] args) {
        // Init gamefield and players
        GameField field = new GameField();

        Player player1 = new Player('X');
        Player player2 = new MiniMax('O');

        // Players are not allowed to have the same symbol
        if (player1.getSymbol() == player2.getSymbol()) throw new IllegalArgumentException("Symbols must be different!");

        while (true) {
            field.print();
            System.out.printf("Player 1 (%c) move:%n", player1.getSymbol());
            field.makeMove(player1.move(field), player1.getSymbol());
            if (field.checkWin(player1.getSymbol())) {
                System.out.println("Player 1 wins!");
                break;
            }

            if (field.checkDraw(player1.getSymbol(), player2.getSymbol())) {
                System.out.println("Draw!");
                break;
            }

            field.print();
            System.out.printf("Player 2 (%c) move:%n", player2.getSymbol());
            field.makeMove(player2.move(field), player2.getSymbol());
            if (field.checkWin(player2.getSymbol())) {
                System.out.println("Player 2 wins!");
                break;
            }

            if (field.checkDraw(player1.getSymbol(), player2.getSymbol())) {
                System.out.println("Draw!");
                break;
            }

            /* Making moves is handled through the Player class.
            *  This allows us to extend the game, for example by making an AI player.*/
        }
    }

    public char intToChar(int i) {
        return switch (i) {
            case 0 -> '0';
            case 1 -> '1';
            case 2 -> '2';
            case 3 -> '3';
            case 4 -> '4';
            case 5 -> '5';
            case 6 -> '6';
            case 7 -> '7';
            case 8 -> '8';
            default -> throw new IllegalStateException("Unexpected value: " + i);
        };
    }
}
