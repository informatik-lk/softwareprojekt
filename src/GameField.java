public class GameField extends Main {
    private final char[] field;

    public GameField() {
        field = new char[]{'0', '1', '2', '3', '4', '5', '6', '7', '8'};
    }

    public boolean validateMove(int move) {
        if (move < 0 || move > 8) {
            return false;
        }

        return field[move] == new Main().intToChar(move);
    }

    public void print() {
        System.out.println("---------");
        for (int i = 0; i < 9; i += 3) {
            System.out.printf("| %c %c %c |\n", field[i], field[i + 1], field[i + 2]);
        }
        System.out.println("---------");
    }

    public void makeMove(int move, char symbol) {
        field[move] = symbol;
    }

    public boolean checkWin(char symbol) {
        int[][] WINNING_COMBINATIONS = {
            {0, 1, 2}, {3, 4, 5}, {6, 7, 8}, // rows
            {0, 3, 6}, {1, 4, 7}, {2, 5, 8}, // columns
            {0, 4, 8}, {2, 4, 6}             // diagonals
        };

        for (int[] combination : WINNING_COMBINATIONS) {
            if (field[combination[0]] == symbol &&
                field[combination[1]] == symbol &&
                field[combination[2]] == symbol) {
                return true;
            }
        }
        return false;
    }

    public boolean checkDraw(char s1, char s2) {
        for (char c : field) {
            if (c != s1 && c != s2) {
                return false;
            }
        }
        return true;
    }
}
