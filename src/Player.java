import java.util.Scanner;

public class Player {
    protected char symbol;

    // The player symbol is set when instantiating the player
    public Player(char symbol) {
        this.symbol = symbol;
    }

    public int move(GameField field) {
        Scanner sc = new Scanner(System.in);
        // Validating the move is partly handled by the player and partly by the gamefield

        int move;
        try {
            move = sc.nextInt();
        } catch (Exception e) {
            System.out.println("Invalid move. Try again.");
            return move(field);
        }

        if (!field.validateMove(move)) {
            System.out.println("Invalid move. Try again.");
            return move(field);
        }

        return move;
    }

    public char getSymbol() {
        return symbol;
    }
}
