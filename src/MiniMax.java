public class MiniMax extends Player {

    public MiniMax(char symbol) {
        super(symbol);
    }

    @Override
    public int move(GameField field) {
        int bestScore = Integer.MIN_VALUE;
        int bestMove = -1;
        for (int i = 0; i < 9; i++) {
            if (field.validateMove(i)) {
                field.makeMove(i, symbol);
                int score = minimax(field, 0, false);
                field.makeMove(i, new Main().intToChar(i));
                if (score > bestScore) {
                    bestScore = score;
                    bestMove = i;
                }
            }
        }
        return bestMove;
    }

    private int minimax(GameField field, int depth, boolean isMaximizing) {
        char opponentSymbol = symbol == 'X' ? 'O' : 'X';
        if (field.checkWin(symbol)) {
            return 10 - depth;
        } else if (field.checkWin(opponentSymbol)) {
            return depth - 10;
        } else if (field.checkDraw(symbol, opponentSymbol)) {
            return 0;
        }

        if (isMaximizing) {
            int bestScore = Integer.MIN_VALUE;
            for (int i = 0; i < 9; i++) {
                if (field.validateMove(i)) {
                    field.makeMove(i, symbol);
                    int score = minimax(field, depth + 1, false);
                    field.makeMove(i, new Main().intToChar(i));
                    bestScore = Math.max(score, bestScore);
                }
            }
            return bestScore;
        } else {
            int bestScore = Integer.MAX_VALUE;
            for (int i = 0; i < 9; i++) {
                if (field.validateMove(i)) {
                    field.makeMove(i, opponentSymbol);
                    int score = minimax(field, depth + 1, true);
                    field.makeMove(i, new Main().intToChar(i));
                    bestScore = Math.min(score, bestScore);
                }
            }
            return bestScore;
        }
    }
}

/* Here is the explanation for the code above:
1. The recursive function minimax() takes three parameters: the current state of the board, the depth of the search, 
and a boolean flag isMaximizing. The depth is used to limit the search depth and isMaximizing is used to determine 
whether the current call is maximizing or minimizing. 
2. The function starts by checking if the game is over by calling the checkWin() and checkDraw() methods.
3. If the game is over, the function returns a score based on the depth. If the game is won by the maximizing player, 
the function returns a positive score, otherwise a negative score. If the game is a draw, the function returns 0.
4. If the game is not over, the function checks if the current call is maximizing or minimizing. If the call is maximizing, 
the function loops through the board and makes a move in all the empty cells. The function then calls itself recursively 
with the new board configuration and depth + 1. It then selects the highest score from the returned values and returns this value.
5. If the call is minimizing, the function loops through the board and makes a move in all the empty cells. The function then 
calls itself recursively with the new board configuration and depth + 1. It then selects the lowest score from the returned 
values and returns this value. */