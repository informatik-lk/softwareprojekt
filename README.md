# Tic-Tac-Toe
This is an awesome Tic-Tac-Toe Game

## TODOs
- [x] Check if non-integer input is given
- [x] Check for draw
- [ ] Maybe even check for draw in next move
- [x] Implement AI Players

## Wie benutze ich das Spiel?
Willkommen zu unserem Tic-Tac-Toe Spiel. Wähle im Code, ob du gegen einen Freund oder gegen unseren unschlagbaren MiniMax spielen möchtest. Ihr setzt abwechselnd eure Symbole und wer zuerst drei Symbole in einer Reihe, Spalte oder Diagonal hat, der gewinnt. 

## Arbeitsteilung
Wir haben uns in der ersten Unterrichtsstunde zu dem Kurzprojekt eine Plan erstellt, welche Klassen wir haben wollen und wie diese Klassen verknüpft sind. So haben wir dann aufgeteilt wer welche Klassen übernimmt. Da hatten sich Taavi um das Gamefield und zusammen mit Arvid um die Main Class gekümmert und Arvid um die Player Class. Jimi hat den beiden mit seinem prüfenden Auge über die Schulter geblickt und bei Fragen sofort geholfen. Nachdem wir aber später noch unserem Spiel einen künstlichen Gegner hinzufügen wollten, hat sich Jimi um den MiniMax gekümmert. Taavi hatte außerdem eine Website erstellt auf der wir das Spiel eigentlich hosten wollten. Die Umsetzunghaben haben wir aus Zeitgründen jedoch nicht mehr hinbekommen.

## Warum ein CLI?
Wir haben uns bewusst gegen die Verwendung eines grafischen Benutzeroberflächen (GUI) für unser Java-Tic-Tac-Toe-Spiel entschieden und stattdessen ein Command Line Interface (CLI) implementiert. Diese Entscheidung basiert auf verschiedenen Überlegungen. Erstens ermöglicht ein CLI eine einfachere und schnellere Implementierung des grundlegenden Spiellogik. Da unser Hauptfokus auf der Kernfunktionalität des Tic-Tac-Toe-Spiels liegt, erschien uns die Entwicklung eines GUI als unnötiger Overhead, der die Projektumsetzung verkomplizieren könnte.

Zweitens bietet ein CLI eine benutzerfreundliche Schnittstelle für Entwickler und Spieler, die sich auf das Spiel selbst konzentrieren möchten, ohne von visuellen Elementen abgelenkt zu werden. Es ermöglicht eine klare Interaktion über die Konsole, was besonders für diejenigen von Vorteil ist, die mit der Programmierung vertraut sind oder das Spiel in einer rein textbasierten Umgebung genießen möchten.

Abschließend ermöglicht die Verwendung eines CLI eine leichtere Skalierbarkeit und Erweiterbarkeit des Codes. Änderungen und Ergänzungen können effizienter vorgenommen werden, ohne komplexe Anpassungen an einer GUI vornehmen zu müssen. Insgesamt erlaubt die Entscheidung für ein CLI eine fokussierte Entwicklung und eine klare Benutzererfahrung, was besonders für ein einfaches Spiel wie Tic-Tac-Toe von Vorteil ist.

Und damit: Viel Spaß mit Tic-Tac-toe!
